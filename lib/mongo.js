const mongoose = require("mongoose");

mongoose
    .connect(`mongodb://localhost:27017/heatSurvie`, {
        dbName: process.env.MONGODB_DBNAME,
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then((result) => console.log("mongo connected"))
    .catch((err) => console.log(err));

module.exports = mongoose.connection;
